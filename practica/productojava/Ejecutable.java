public class Ejecutable{
    public static void main( String[] args){
        Producto producto1=new Producto();
        Producto producto2=new Producto();

        producto1.setCodigoDeProducto(123);
        producto1.setNombre("Libro");
        producto1.setPrecioDeCosto(30.00);
        producto1.setPorcentajeDeGanancia(3);

        producto1.calculoPrecioDeVenta();

        producto1.imprimirDatos();

        producto2.setCodigoDeProducto(567);
        producto2.setNombre("Carpeta");
        producto2.setPrecioDeCosto(15.00);
        producto2.setPorcentajeDeGanancia(7);

        producto2.calculoPrecioDeVenta();

       producto2.imprimirDatos();

        producto1.comparar(producto2.getPrecioDeVenta());

    }
}