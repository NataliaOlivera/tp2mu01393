public class Producto{
    private int codigoDeProducto;
    private String nombre;
    private Double precioDeCosto;
    private int porcentajeDeGanancia;
    private int iva=21;
    private Double precioDeVenta;

    public void setCodigoDeProducto(int codigoDeProducto){
        this.codigoDeProducto=codigoDeProducto;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }

    public void setPrecioDeCosto(Double precioDeCosto){
        this.precioDeCosto=precioDeCosto;
    }

    public void setPorcentajeDeGanancia(int porcentajeDeGanancia){
        this.precioDeCosto=precioDeCosto;
    }

    public void setPrecioDeventa(Double precioDeVenta){
        this.precioDeVenta=precioDeVenta;
    }


    public Double calculoPrecioDeVenta(){
        precioDeVenta=precioDeCosto+(iva/100)*precioDeCosto+porcentajeDeGanancia*precioDeCosto;
        return precioDeVenta;
    }


    public int getCodigoDeProducto(){
        return codigoDeProducto;
    }

    public String getNombre(){
        return nombre;
    }

    public Double getPrecioDeCosto(){
        return precioDeCosto;
    }

    public int getPorcentajeGanancia(){
        return porcentajeDeGanancia;
    }

    public int getIva(){
        return iva;
    }

    public Double getPrecioDeVenta(){
        return precioDeVenta;
    }


   

    public void comparar(Double precio){
        Double precio1=this.getPrecioDeVenta();
        Double precio2=precio;

        if(precio1>precio2){
            System.out.println("El precio del primer producto es mayor");
        }
        else{
            if(precio2>precio1){
                System.out.println("El precio del segundo producto es mayor");
            }
            else{
                System.out.println("Ambos precios son iguales");
                
            }

        }
        
    }


    public void imprimirDatos()
    {
        System.out.println("Codigo del producto:  "+getCodigoDeProducto()+ "  Producto:  "+getNombre()+ "  Precio de Venta:  "+getPrecioDeVenta());
        
    }

}