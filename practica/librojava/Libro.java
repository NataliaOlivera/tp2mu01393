public class Libro{
    private String isbn;
    private String titulo;
    private String autor;
    private int numeroDePaginas;

    public void setIsbn(String isbn){
        this.isbn=isbn;
    }

    public void setTitulo(String titulo){
        this.titulo=titulo;
    }

    public void setAutor(String autor){
        this.autor=autor;
    }

    public void setNumeroDePaginas(int numeroDePaginas){
        this.numeroDePaginas=numeroDePaginas;
    }

    public String getIsbn(){
        return isbn;
    }

    public String getTitulo(){
        return titulo;
    }

    public String getAutor(){
        return autor;
    }

    public int getNumeroDePaginas(){
        return numeroDePaginas;
    }
    

}