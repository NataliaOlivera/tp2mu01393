public class Ejecutable{
    public static void main( String[] args){
        Libro libro1=new Libro();
        Libro libro2=new Libro();

        libro1.setIsbn("1234");
        libro1.setTitulo("Libro de auto ayuda");
        libro1.setAutor("Natalia Olivera");
        libro1.setNumeroDePaginas(22);

        System.out.println("El libro con ISBN es: "+libro1.getIsbn()+ "  De Titulo:  "+libro1.getTitulo()+ "  creado por el Autor:  "+libro1.getAutor()+ "  tiene  "+libro1.getNumeroDePaginas()+ " paginas.");

        libro2.setIsbn("4321");
        libro2.setTitulo("Libro de Programacion");
        libro2.setAutor("Java cc");
        libro2.setNumeroDePaginas(123);

        System.out.println("El libro con ISBN es: "+libro2.getIsbn()+ "  De Titulo:  "+libro2.getTitulo()+ "  creado por el Autor:  "+libro2.getAutor()+ "  tiene  "+libro2.getNumeroDePaginas()+ " paginas.");


    }
}